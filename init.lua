-- Namespace
zoonami_3d_mobs = {}

-- Return mob data
function zoonami_3d_mobs.mobs()
	return dofile(minetest.get_modpath("zoonami_3d_mobs") .. "/lua/mobs.lua")
end
