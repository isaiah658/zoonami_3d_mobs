# Zoonami 3D Mobs
A Minetest mod that changes the 2D monsters mobs in [Zoonami](https://codeberg.org/isaiah658/zoonami) to be 3D. The 3D models are not animated.

[Version 1.2.0](https://codeberg.org/isaiah658/zoonami_3d_mobs/src/branch/main/CHANGELOG.md)

## Links
* [Zoonami Minetest Forum Topic](https://forum.minetest.net/viewtopic.php?f=9&t=25356&sid=1ffebc6a6c8b35653d939a376a067a7f)
* [Zoonami 3D Mobs Source Code](https://codeberg.org/isaiah658/zoonami_3d_mobs)

## Licensing
Licensing can be found in the [LICENSE.txt](https://codeberg.org/isaiah658/zoonami_3d_mobs/src/branch/main/LICENSE.txt) file.
