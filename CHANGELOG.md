# Zoonami 3D Mobs Changelog

## Version 1.2.0
* Added 3 new monsters for the Zoonami v1.3.0 release

## Version 1.1.0
* Added 3 new monsters for the Zoonami v1.1.0 release

## Version 1.0.0
* Added 6 new monsters for the Zoonami v1.0.0 release

## Version 0.6.0
* Added 4 new monsters for the Zoonami v0.15.0 release

## Version 0.5.0
* Added 7 new monsters for the Zoonami v0.14.0 release
* Added prisma colors for all monsters
* Further compressed PNGs

## Version 0.4.1
* Optimized the OBJ files by removing extraneous zeros (122702 bytes saved - 7.89% decrease)

## Version 0.4.0
* Added 3 new monsters for the Zoonami v0.11.0 release

## Version 0.3.0
* Added 7 new monsters for the Zoonami v0.10.0 release
* Adjusted some of the visual and hitbox sizes to make the 3D monsters and 2D monsters closer in size

## Version 0.2.1
* Fixed missing tail on Howler

## Version 0.2.0
* Added 3 new monsters for the Zoonami v0.9.2 release

## Version 0.1.0
* Initial release
* Adds 3D mobs for the 20 monsters currently in Zoonami v0.9.0